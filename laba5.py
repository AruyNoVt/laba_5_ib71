# -*- coding: utf-8 -*-

import vk_api
import viginer as v, caesar as c
import qrcode
import time, requests, os
from PIL import Image, ImageFilter
from vk_api.longpoll import VkLongPoll, VkEventType

vk_session = vk_api.VkApi(token='1de45b5216b95c434e51b4d22474412087c196d6acf2553b449c81acb47574f4ba7cec66a151d0034f41b')
vk = vk_session.get_api()

STEP_MAIN = 0
STEP_TYPE = 1
STEP_CIPHER_TYPE = 2
STEP_CIPHER_KEY = 3
STEP_CIPHER_TEXT = 4
STEP_QRCODE = 5
STEP_LOADIMG = 6

class user:
    uid = 0
    step = 0
    atype = 0 # 0 - caesar, 1 - viginer, 2 - QR-Code
    crypt = False # True = crypt, False = decrypt
    key = ''
    def __init__(self, user_id):
        self.uid = user_id

def send_msg(userid, msg_txt='', msg_photos=None):
    if msg_photos != None:
        upload = vk_api.VkUpload(vk_session)
        uploaded_photos = upload.photo_messages(msg_photos)
        attachments = []
        for photo in uploaded_photos:
            attachments.append('photo{}_{}'.format(photo['owner_id'], photo['id']))
        msg_photos = ','.join(attachments)

    vk.messages.send(
        user_id=userid,
        message=msg_txt,
        attachment=msg_photos,
        )

def get_img_size(sizes, size_type):
    for size in sizes:
        if size['type'] == size_type:
            return size

def parse_json_format(attachments):
    urls = dict()
    for attach in attachments:
        photo_id = attach['photo']['id']
        size = None
        for size_type in ['w', 'y', 'z', 'x', 'm', 's']:
            size = get_img_size(attach['photo']['sizes'], size_type)
            if size is not None:
                break
        url = size['url']
        urls[photo_id] = url
    return urls

def download(fileLink):
    fname = str(time.time()) + 'savedFile.jpg'
    with open(fname, 'wb') as handle:
        response = requests.get(fileLink, stream=True)
        if not response.ok:
            print(response)
        for block in response.iter_content(1024):
            if not block:
                break
            handle.write(block)
    return fname

def main():
    longpoll = VkLongPoll(vk_session)
    userList = []
	
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                idx = -1

                for i in range(0, len(userList)):
                    if userList[i].uid == event.user_id:
                        idx = i
                        break

                print(event.peer_id)
                    
                if idx == -1:
                    idx = len(userList)
                    userList.append(user(event.user_id))

                if userList[idx].step == STEP_MAIN:
                    send_msg(event.user_id, 'Вас приветствует бот.\n\
                        Выберите, что вы хотите сделать:\n1 - использовать шифр Цезаря;\n2 - использовать шифр Вижинера;\n3 - сгенерировать QR-код\n4 - загрузить и размыть фотографии')
                    userList[idx].step = STEP_TYPE

                elif userList[idx].step == STEP_TYPE:
                    cipherid = 0
                    if event.text.isnumeric():
                        cipherid = int(event.text)
                    if cipherid in range(1,5):
                        userList[idx].atype = cipherid
                        ans = ''
                        if cipherid == 1: ans = 'Вы выбрали шифр Цезаря.'
                        elif cipherid == 2: ans = 'Вы выбрали шифр Вижинера.'
                        elif cipherid == 3: ans ='Вы выбрали генерацию QR-кода.\nВведите текст, который необходимо преобразовать:'
                        else: ans = 'Вы выбрали загрузку и размытие фотографий.\nЗагрузите фотографии, чтобы преобразовать их:'
                        
                        if cipherid == 3: userList[idx].step = STEP_QRCODE
                        elif cipherid == 4: userList[idx].step = STEP_LOADIMG
                        else:
                            ans = ans + '\nКакую операцию следует выполнить?\n1 - шифрование;\n2 - дешифрование'
                            userList[idx].step = STEP_CIPHER_TYPE
                        send_msg(event.user_id, ans)
                    else:
                        send_msg(event.user_id, 'Такого действия не существует. Повторите попытку:\n1 - шифр Цезаря;\n2 - шифр Вижинера;\n3 - генерация QR-кода\n4 - загрузка и размытие фотографий')

                elif userList[idx].step == STEP_CIPHER_TYPE:
                    actionid = 0
                    if event.text.isnumeric():
                        actionid = int(event.text)
                    if actionid in range(1,3):
                        ans = ''
                        if actionid == 1:
                            userList[idx].crypt = True
                            ans ='Вы выбрали операцию шифрования.'
                        else:
                            userList[idx].crypt = False
                            ans = 'Вы выбрали операцию дешифрования'
                        if userList[idx].atype == 1:
                            userList[idx].step = STEP_CIPHER_TEXT
                            ans = ans + '\nВведите текст, который необходимо преобразовать:'
                        else:
                            userList[idx].step = STEP_CIPHER_KEY
                            ans = ans + '\nВведите ключ, который необходимо использовать при преобразовании:'
                        send_msg(event.user_id, ans)
                    else:
                        send_msg(event.user_id, 'Такого действия не существует. Повторите попытку:\n1 - шифрование;\n2 - дешифрование')

                elif userList[idx].step == STEP_CIPHER_KEY:
                    if len(event.text) > 0:
                        userList[idx].step = STEP_CIPHER_TEXT
                        userList[idx].key = event.text
                        send_msg(event.user_id, 'Вы ввели ключ. Введите текст, который необходимо преобразовать:')
                    else:
                        send_msg(event.user_id, 'Произошла ошибка. Повторите попытку ввода текста-ключа:')

                elif userList[idx].step == STEP_CIPHER_TEXT:
                    if len(event.text) > 0:
                        ans = ''
                        if userList[idx].atype == 1:
                            if userList[idx].crypt == True: ans = c.encrypt(event.text)
                            else: ans = c.decrypt(event.text)
                        else:
                            if userList[idx].crypt == True: ans = v.encrypt(event.text, userList[idx].key)
                            else: ans = v.decrypt(event.text, userList[idx].key)    
                        send_msg(event.user_id, 'Текст успешно преобразован:\n' + ans)
                        del userList[idx]
                    else:
                        send_msg(event.user_id, 'Произошла ошибка. Повторите попытку ввода текст, небходимого для преобразования:')

                elif userList[idx].step == STEP_QRCODE:
                    if len(event.text) > 0:
                        img = qrcode.make(event.text)
                        img.save('qrphoto.png')
                        img = Image.open('qrphoto.png')
                        img.close()
                        send_msg(event.user_id, 'Текст успешно преобразован:', 'qrphoto.png')
                        os.remove('qrphoto.png')
                        del userList[idx]
                    else:
                        send_msg(event.user_id, 'Произошла ошибка. Повторите попытку ввода текст, небходимого для преобразования:')

                elif userList[idx].step == STEP_LOADIMG:
                    if event.attachments != {}:
                        photo_ids = []
                        k = list(event.attachments.keys())
                        for item_type, item_value in event.attachments.items():
                            if item_value == "photo":
                                photo_ids.append(event.attachments[k[k.index(item_type) + 1]])
                        if len(photo_ids) > 0:
                            jsonoutput = vk_session.method('messages.getHistory', {'user_id':event.peer_id,'count':1})['items'][0]['attachments']
                            urls_lst = parse_json_format(jsonoutput)

                            photo_lst = []
                            for photo_id in photo_ids:
                                pid = int(photo_id.split(str(event.user_id) + "_")[1])
                                if pid in urls_lst:
                                    photo_lst.append( download(urls_lst[pid]) )
                            for photo in photo_lst:
                                img = Image.open(photo)
                                img = img.filter(ImageFilter.BoxBlur(15))
                                img.save(photo)
                                img.close()
                            send_msg(event.user_id, 'Фотографии успешно преобразованы:', photo_lst)
                            for photo in photo_lst:
                                os.remove(photo)
                            del userList[idx]
                        else:
                            send_msg(event.user_id, 'Произошла ошибка. Прикрепите фотографии к вашему сообщению.')
                    else:
                        send_msg(event.user_id, 'Произошла ошибка. Прикрепите фотографии к вашему сообщению.')

if __name__ == '__main__':
    main()
